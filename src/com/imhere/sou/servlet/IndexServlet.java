package com.imhere.sou.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.imhere.sou.util.DeviceUtil;

/**
 * 视图处理Servlet
 * @author imhere
 *
 */
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public IndexServlet() {
		super();
	}

	public void destroy() {
		super.destroy();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//判断请求来源（移动端||电脑端）
		boolean isMobile = DeviceUtil.isRequestFromMobile(request);
		
		if (isMobile) {
			request.getRequestDispatcher("mobile.html").forward(request, response);
		} else {
			request.getRequestDispatcher("index.html").forward(request, response);
		}
	}

	public void init() throws ServletException {
		//do nothing
	}

}
