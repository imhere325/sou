package com.imhere.sou.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.imhere.sou.util.DeviceUtil;

/**
 * 搜索结果页视图
 * @author imhere
 *
 */
public class PanSouServlet extends HttpServlet {
	private static final long serialVersionUID = 8965015444811457015L;

	public PanSouServlet() {
		super();
	}

	public void destroy() {
		super.destroy();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String keyword = (String) request.getParameter("keyword");
		if(keyword != null) request.setAttribute("keyword", keyword);
		boolean isMobile = DeviceUtil.isRequestFromMobile(request);
		if (isMobile) {
			request.getRequestDispatcher("result.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("home.jsp").forward(request, response);
		}
	}

	public void init() throws ServletException {
		
	}
}
